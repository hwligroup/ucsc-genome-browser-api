= UCSC Genome Browser API

This is an ongoing practice project where I am trying to build a REST API that serves data from
the https://genome.ucsc.edu/goldenPath/help/mysql.html[UCSC Genome Browser Mariadb database],
hoping to learn how to use the Gin framework and how to retrieve data from SQL databases.

The API endpoints were intentionally made not to follow 
https://genome.ucsc.edu/goldenPath/help/api.html[the official API],
so that I can try to adhere to some suggested best practices, e.g. use nouns in the URI,
and think about how to represent the relationship in the data.

See the license page of UCSC Genome Browser
https://genome.ucsc.edu/license/

== Currently available endpoints
|===
|`/assemblies`                       | Get a full list of genome assembly info, including the name, the description, the organism, and the data source
|`/assemblies/:assembly`             | Get the info for a specific assembly by name
|`/assemblies/:assembly/chromosomes` | Get a list of chromosomes in an assembly
|===
