package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
)

var db *sql.DB

func main() {
	var err error
	cfg := mysql.NewConfig()
	cfg.User = "genome"
	cfg.Net = "tcp"
	cfg.Addr = "genome-mysql.soe.ucsc.edu:3306"

	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	router := gin.Default()
	router.GET("/assemblies", getAssemblies)
	router.GET("/assemblies/:assembly", getAssembliesByID)
	router.GET("/assemblies/:assembly/chromosomes", getChromosomes)
	router.Run("localhost:8080")

}

type assembly struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Organism    string `json:"organism"`
	Source      string `json:"source"`
}

func getAssemblies(c *gin.Context) {
	//  [name description nibPath organism defaultPos active orderKey genome scientificName htmlPath hgNearOk hgPbOk sourceName taxId]
	rows, err := db.Query("SELECT name, description, organism, sourceName FROM hgcentral.dbDb ")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	defer rows.Close()

	var assemblies []assembly

	for rows.Next() {
		var newAssembly assembly
		err = rows.Scan(&newAssembly.Name, &newAssembly.Description, &newAssembly.Organism, &newAssembly.Source)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": err})
			return
		}
		assemblies = append(assemblies, newAssembly)
	}
	c.JSON(http.StatusOK, assemblies)
}

func getAssembliesByID(c *gin.Context) {
	id := c.Param("assembly")
	row := db.QueryRow("SELECT name, description, organism, sourceName FROM hgcentral.dbDb WHERE name = ?", id)
	var newAssembly assembly
	err := row.Scan(&newAssembly.Name, &newAssembly.Description, &newAssembly.Organism, &newAssembly.Source)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	c.JSON(http.StatusOK, newAssembly)
}

type chromosome struct {
	Name string `json:"name"`
	Size int    `json:"size"`
}

func getChromosomes(c *gin.Context) {
	assembly := c.Param("assembly")
	q := fmt.Sprintf("SELECT chrom, size FROM %s.chromInfo", assembly)
	rows, err := db.Query(q)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	defer rows.Close()
	var chromosomes []chromosome
	for rows.Next() {
		var newChromosome chromosome
		err = rows.Scan(&newChromosome.Name, &newChromosome.Size)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": err})
			return
		}
		chromosomes = append(chromosomes, newChromosome)
	}
	c.JSON(http.StatusOK, chromosomes)
}
